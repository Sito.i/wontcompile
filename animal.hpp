#ifndef __ANIMAL_HPP__
#define __ANIMAL_HPP__

#include "genome.hpp"

namespace Penna {

class Animal {
    public:
        //Animal() : pregnant_(0), age_(0), genome_(Genome()) {}
        
        bool is_pregnant();
        age_t age();
        bool is_dead() const;
        Animal give_birth();


    private:
        static age_t reporoduction_age_;

        bool pregnant_;
        age_t age_;
        Genome const genome_;
};

}; // Penna namespace

#endif // __ANIMAL_HPP__
