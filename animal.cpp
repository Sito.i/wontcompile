#include "animal.hpp"

namespace Penna {

age_t Animal::reporoduction_age_;

bool Animal::is_pregnant(){
    return pregnant_;
}

age_t Animal::age(){
    return age_;
}

bool Animal::is_dead() const{
    return (age_ > Genome::genome_size) 
            or (genome_.count_bad(age_) > genome_.get_threshold());
}

}; // Penna namespace

#endif // _ANIMAL_HPP_