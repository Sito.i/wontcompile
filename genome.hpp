#ifndef __GENOME_HPP__
#define __GENOME_HPP__

#include <bitset>

namespace Penna{

using age_t = unsigned int;

class Genome{
	public:
		static age_t const genome_size = 64;

		Genome() {
			for(std::size_t k = 0; k < genome_size; ++k){
				genes_[k] = false;
			}
		}

		static void  set_mutation_rate(age_t);
		static age_t get_mutation_rate();

		static void  set_threshold(age_t);
		static age_t get_threshold();

		void mutate();

		age_t count_bad(age_t) const;

	private:
		static age_t bad_threshold_;
		static age_t mutation_rate_;
		std::bitset<genome_size> genes_;
};

} // Penna namespace

#endif // _GENOME_HPP_